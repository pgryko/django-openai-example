from django.urls import path

from . import views

app_name = "openapi"
urlpatterns = [
    path("completion", views.stream),
    path("completionChat", views.streamChat),
    path("", views.home, name="home"),
]
