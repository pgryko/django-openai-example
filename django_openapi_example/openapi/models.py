from django.db import models


class Prompt(models.Model):
    prompt = models.TextField()
    is_default = models.BooleanField(default=False)

    def __str__(self):
        return self.prompt

    def save(self, *args, **kwargs):
        if self.is_default:
            Prompt.objects.exclude(pk=self.pk).update(is_default=False)
        super().save(*args, **kwargs)
