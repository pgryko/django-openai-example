import openai
from django.conf import settings
from django.http import StreamingHttpResponse
from django.shortcuts import render


def home(request):
    if request.method == "POST":
        user_input = request.POST.get("user_input")
        api_response = make_openapi_request(user_input)
        context = {"api_response": api_response}
        return render(request, "openapi/home.html", context)
    else:
        return render(request, "openapi/home.html")


def make_openapi_request(text: str):
    # Make the POST request to the OpenAPI endpoint
    openai.api_key = settings.OPENAI_API_KEY

    prompt = """The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly."""

    response = openai.ChatCompletion.create(
        # model="text-davinci-002-render-sha",
        model="gpt-3.5-turbo-0301",
        messages=[
            {"role": "system", "content": prompt},
            {"role": "user", "content": text},
        ],
        # stream=True
    )

    # Return the response from the OpenAPI endpoint
    if response:
        return response["choices"][0]["message"]["content"]
    else:
        return {"error": "Failed to make OpenAPI request"}


def stream(request):
    def event_stream():
        completion = openai.Completion.create(
            engine="text-davinci-003", prompt="Hello world", stream=True
        )
        for line in completion:
            yield "data: %s\n\n" % line.choices[0].text

    return StreamingHttpResponse(event_stream(), content_type="text/event-stream")


def streamChat(request):
    def event_stream():
        completion = openai.ChatCompletion.create(
            model="gpt-3.5-turbo",
            messages=[{"role": "user", "content": "Hello world"}],
            stream=True,
        )
        for line in completion:
            chunk = line["choices"][0].get("delta", {}).get("content", "")
            if chunk:
                yield "data: %s\n\n" % chunk

    return StreamingHttpResponse(event_stream(), content_type="text/event-stream")
